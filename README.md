## Restfull API Product
By Rama Adhitya Setiadi

## Credits

- Framework : Laravel 8

## Requirement

- PHP Version 8
- MySQL

## Installation

1. First clone or download this repository:
```bash
git clone https://gitlab.com/ramaadit123/rest-api-laravel.git
```

2. Setting the database configuration in `.env` and open file at project root directory
```bash
DB_DATABASE=**your_db_name**
DB_USERNAME=**your_db_user**
DB_PASSWORD=**password**
```

3. Run Migration Database the following command at the terminal:
```bash
 php artisan migrate:fresh --seed
```

4. Run laravel
```bash
php artisan serve
```

## Login User
1. Admin
```bash
email : rama@gmail.com
password : password
```
2. Seller
```bash
email : rifa@gmail.com
password : password
```
3. User
```bash
email : bunga@gmail.com
password : password
```

### Backend API DOCs 

https://documenter.getpostman.com/view/13756818/2s935hQSW3
