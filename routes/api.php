<?php

use App\Http\Controllers\{CategoryController, ImageController, ProductController, SearchController, TokenGeneratorController, UserController, VariantController};
use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum')->group(function () {
    Route::get('user', [UserController::class, 'show']);
});

Route::post('token/generator', TokenGeneratorController::class);
Route::get('search', SearchController::class);
Route::apiResource('products', ProductController::class);
Route::apiResource('categories', CategoryController::class);

Route::prefix('variants')->group(function () {
    Route::get('', [VariantController::class, 'index']);
    Route::get('{product:slug}/{variant:slug}', [VariantController::class, 'show']);
    Route::post('{product:slug}', [VariantController::class, 'store']);
    Route::delete('{variant:slug}', [VariantController::class, 'destroy']);
    Route::put('{product:slug}/{variant:slug}/edit', [VariantController::class, 'update']);
});

Route::prefix('images')->group(function () {
    Route::get('', [ImageController::class, 'index']);
    Route::post('{product:slug}', [ImageController::class, 'store']);
    Route::get('{product:slug}/{image:slug}', [ImageController::class, 'show']);
    Route::delete('{product:slug}/{image:slug}', [ImageController::class, 'destroy']);
});
