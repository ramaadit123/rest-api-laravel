<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    use HasFactory;

    protected $fillable = ['category_id', 'variant_id', 'name', 'slug', 'description', 'condition', 'price', 'stock'];
    // protected $with = ['category', 'variants', 'images'];

    public static function booted()
    {
        static::creating(function (Product $product) {
            $product->slug = strtolower(Str::slug($product->name) . '-' . str::random(6));
        });
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function variants()
    {
        return $this->hasMany(Variant::class);
    }

    public function images()
    {
        return $this->hasMany(Image::class);
    }
}
