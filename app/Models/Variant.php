<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Variant extends Model
{
    use HasFactory;

    protected $guarded = [];
    // protected $with = ['product'];

    public static function booted()
    {
        static::creating(function (Variant $variant) {
            $variant->slug = strtolower(Str::slug(Str::random(6)));
        });
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
