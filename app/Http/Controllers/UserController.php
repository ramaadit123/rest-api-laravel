<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function show(Request $request)
    {
        return (new UserResource($request->user())
        )->additional([
            'role' => [
                'admin' => $request->user()->hasRole('admin'),
                'seller' => $request->user()->hasRole('seller'),
            ],
        ]);
    }
}
