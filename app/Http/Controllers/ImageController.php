<?php

namespace App\Http\Controllers;

use App\Models\{Image, Product};
use App\Http\Requests\ImageRequest;
use App\Http\Resources\ImageResource;
use Illuminate\Support\Facades\Storage;
use App\Interfaces\ImageRepositoryInterface;
use Illuminate\Http\{JsonResponse, Request, Response};

class ImageController extends Controller
{
    private ImageRepositoryInterface $imageRepository;
    public function __construct(ImageRepositoryInterface $imageRepository)
    {
        $this->middleware('auth:sanctum')->except(['index', 'show']);
        $this->imageRepository = $imageRepository;
    }

    public function index()
    {
        $image = Image::latest()->paginate(8);
        return ImageResource::collection($image);
    }

    public function store(ImageRequest $request, Product $product): JsonResponse
    {
        $this->authorize('if_seller');

        return response()->json([
            'message' => "Image Product $product->name was created!",
            'data' => $this->imageRepository->store($product, $request),
        ], Response::HTTP_CREATED);
    }

    public function show(Product $product, Image $image): JsonResponse
    {
        return response()->json([
            'message' => "Show image product $product->name",
            'data' => $this->imageRepository->show($image)
        ]);
    }

    public function destroy(Product $product, Image $image): JsonResponse
    {
        $this->authorize('if_seller');

        $this->imageRepository->delete($image);
        return response()->json(['message' => "Image product $product->name was deleted!"]);
    }
}
