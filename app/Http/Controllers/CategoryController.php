<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Http\Requests\CategoryRequest;
use App\Http\Resources\CategoryResource;
use App\Interfaces\CategoryRepositoryInterface;
use Illuminate\Http\Response;

class CategoryController extends Controller
{
    private CategoryRepositoryInterface $CategoryRepository;
    public function __construct(CategoryRepositoryInterface $CategoryRepository)
    {
        $this->middleware('auth:sanctum')->except(['index', 'show']);
        $this->CategoryRepository = $CategoryRepository;
    }

    public function index()
    {
        $categories = Category::latest()->paginate(8);
        return CategoryResource::collection($categories);
    }

    public function store(CategoryRequest $request)
    {
        $this->authorize('if_admin');

        return response()->json([
            'message' => "Category $request->name was created!",
            'data' => $this->CategoryRepository->store($request)
        ], Response::HTTP_CREATED);
    }

    public function show(Category $category)
    {
        return response()->json([
            'message' => "Show Category $category->name",
            'data' => $this->CategoryRepository->show($category)
        ]);
    }

    public function update(CategoryRequest $request, Category $category)
    {
        $this->authorize('if_admin');

        return response()->json([
            'message' => "Category $category->name was updated!",
            'data' => $this->CategoryRepository->update($category, $request)
        ]);
    }

    public function destroy(Category $category)
    {
        $this->authorize('if_admin');

        $this->CategoryRepository->delete($category);
        return response()->json(['message' => "Category $category->name was deleted!",]);
    }
}
