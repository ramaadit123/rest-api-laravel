<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use App\Models\{Product, Variant};
use App\Http\Requests\VariantRequest;
use App\Http\Resources\VariantResource;
use App\Interfaces\VariantRepositoryInterface;

class VariantController extends Controller
{
    private VariantRepositoryInterface $variantRepository;
    public function __construct(VariantRepositoryInterface $variantRepository)
    {
        $this->middleware('auth:sanctum')->except(['index', 'show']);
        $this->variantRepository = $variantRepository;
    }

    public function index()
    {
        $variants = Variant::latest()->paginate(10);
        return VariantResource::collection($variants);
    }

    public function store(VariantRequest $request, Product $product)
    {
        $this->authorize('if_seller');

        return response()->json([
            'message' => "Variant $request->options $request->value was created!",
            'data' => $this->variantRepository->store($product, $request),
        ], Response::HTTP_CREATED);
    }

    public function show(Product $product, Variant $variant)
    {
        return response()->json([
            'message' => "Show variant $variant->options $variant->value",
            'data' => $this->variantRepository->show($variant),
        ]);
    }

    public function update(Product $product, Variant $variant, VariantRequest $request)
    {
        $this->authorize('if_seller');

        return response()->json([
            'message' => "Variant $variant->options $variant->value was updated!",
            'data' => $this->variantRepository->update($variant, $request)
        ]);
    }

    public function destroy(Variant $variant)
    {
        $this->authorize('if_seller');

        $this->variantRepository->delete($variant);
        return response()->json(['message' => 'Variant was deleted!']);
    }
}
