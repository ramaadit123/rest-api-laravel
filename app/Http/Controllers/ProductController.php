<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Response;
use App\Http\Requests\ProductRequest;
use App\Http\Resources\ProductResource;
use App\Interfaces\ProductRepositoryInterface;

class ProductController extends Controller
{
    private ProductRepositoryInterface $productRepository;
    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->middleware('auth:sanctum')->except(['index', 'show']);
        $this->productRepository = $productRepository;
    }

    public function index()
    {
        $product = Product::latest()->paginate(10);
        return ProductResource::collection($product);
    }

    public function store(ProductRequest $request)
    {
        $this->authorize('if_seller');

        return response()->json([
            'message' => "Product $request->name was created!",
            'data' => $this->productRepository->store($request),
        ], Response::HTTP_CREATED);
    }

    public function show(Product $product)
    {
        return response()->json([
            'message' => "Show product $product->name",
            'data' => $this->productRepository->show($product)
        ]);
    }

    public function update(ProductRequest $request, Product $product)
    {
        $this->authorize('if_seller');

        return response()->json([
            'message' => "Product $product->name was updated!",
            'data' => $this->productRepository->update($product, $request)
        ]);
    }

    public function destroy(Product $product)
    {
        $this->authorize('if_seller');

        $this->productRepository->delete($product);
        return response()->json(['message' => "Product $product->name was deleted!"]);
    }
}
