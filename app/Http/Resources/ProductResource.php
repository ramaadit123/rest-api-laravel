<?php

namespace App\Http\Resources;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'slug'          => $this->slug,
            'price'         => [
                'formatted'     => number_format($this->price, 0, '.', '.'),
                'unformatted'   => $this->price,
            ],
            'category'      => [
                'id'        => $this->id,
                'name'      => $this->category->name,
                'slug'      => $this->category->slug,
            ],
            'thumbnail'     => $this->thumbnail ? Storage::url($this->thumbnail) : null,
            'image'         => $this->images->map(fn ($image) => [
                'id'        => $image->id,
                'image'     => $image->image,
            ]),
            'created'       => $this->created_at->format('d F Y'),
        ];
    }
}
