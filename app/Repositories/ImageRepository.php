<?php

namespace App\Repositories;

use App\Models\Image;
use Illuminate\Support\Str;
use App\Http\Resources\ImageResource;
use App\Interfaces\ImageRepositoryInterface;
use Illuminate\Support\Facades\Storage;

class ImageRepository implements ImageRepositoryInterface
{
    public function getAllImage($image)
    {
        $image = Image::latest()->paginate(10);
        return ImageResource::collection($image);
    }

    public function store($product, $request)
    {
        $image = $request->file('image');
        $attributes = $request->all();
        $attributes['image'] = $request->hasFile('image') ? $image->store('images/product') : null;
        $data = $product->images()->create($attributes);
        return new ImageResource($data);
    }

    public function show($image)
    {
        return new ImageResource($image);
    }

    public function delete($image)
    {
        if ($image->image) {
            Storage::delete($image->image);
        }

        $image->delete();
    }
}
