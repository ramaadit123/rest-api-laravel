<?php

namespace App\Repositories;

use App\Models\Product;
use Illuminate\Support\Str;
use App\Http\Resources\ProductResource;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\ProductSingleResource;
use App\Interfaces\ProductRepositoryInterface;
use App\Models\Variant;

class ProductRepository implements ProductRepositoryInterface
{
    public function getAllProducts($product)
    {
        $product = Product::latest()->paginate(10);
        return ProductResource::collection($product);
    }

    public function store($request)
    {
        $thumbnail = $request->file('thumbnail');
        $attributes = $request->all();
        $attributes['thumbnail'] = $request->hasFile('thumbnail') ? $thumbnail->store('images/thumbnail/product') : null;
        $data = $request->user()->products()->create($attributes);
        return new ProductSingleResource($data);
    }

    public function show($product)
    {
        return new ProductSingleResource($product);
    }

    public function update($product, $request)
    {
        if ($request->has('thumbnail') && $request->file('thumbnail')->isValid()) {
            Storage::delete($product->thumbnail);
        }

        $attributes = $request->all();
        $attributes['slug'] = strtolower(Str::slug($product->name) . '-' . str::random(6));
        $product->update($attributes);
        return new ProductSingleResource($product);
    }

    public function delete($product)
    {
        if ($product->thumbnail) {
            Storage::delete($product->thumbnail);
        }
        $product->variants()->delete();
        $product->images()->delete();
        $product->delete();
    }
}
