<?php

namespace App\Repositories;

use Illuminate\Support\Str;
use App\Http\Resources\VariantResource;
use App\Interfaces\VariantRepositoryInterface;
use App\Models\Variant;
use Illuminate\Support\Facades\Storage;

class VariantRepository implements VariantRepositoryInterface
{
    public function getAllVariant($variant)
    {
        $variant = Variant::latest()->paginate(10);
        return VariantResource::collection($variant);
    }

    public function store($product, $request)
    {
        $image = $request->file('image');
        $attributes = $request->all();
        $attributes['image'] = $request->hasFile('image') ? $image->store('images/variant') : null;
        $data = $product->variants()->create($attributes);
        return new VariantResource($data);
    }

    public function show($variant)
    {
        return new VariantResource($variant);
    }

    public function update($variant, $request)
    {
        if ($request->has('image') && $request->file('image')->isValid()) {
            Storage::delete($variant->image);
        }

        $image = $request->file('image');
        $attributes = $request->all();
        $attributes['image'] = $request->hasFile('image') ? $image->store('images/variant') : null;
        $attributes['slug'] = strtolower(Str::slug(Str::random(6)));
        $variant->update($attributes);
        return new VariantResource($variant);
    }

    public function delete($variant)
    {
        if ($variant->image) {
            Storage::delete($variant->image);
        }
        $variant->delete();
    }
}
