<?php

namespace App\Repositories;

use Illuminate\Support\Str;
use App\Http\Resources\CategoryResource;
use App\Interfaces\CategoryRepositoryInterface;
use App\Models\Category;

class CategoryRepository implements CategoryRepositoryInterface
{
    public function fetchAll($category)
    {
        $category = Category::latest()->paginate(10);
        return CategoryResource::collection($category);
    }

    public function store($request)
    {
        $data = Category::create($request->all());
        return new CategoryResource($data);
    }

    public function show($category)
    {
        return new CategoryResource($category);
    }

    public function update($category, $request)
    {
        $request['slug'] = strtolower(Str::slug($request->name . '-' . Str::random(6)));
        $category->update($request->all());
        return new CategoryResource($category);
    }

    public function delete($category)
    {
        $category->delete();
    }
}
