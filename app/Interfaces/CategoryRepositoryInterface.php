<?php

namespace App\Interfaces;

interface CategoryRepositoryInterface
{
    public function fetchAll($category);
    public function store($request);
    public function show($category);
    public function update($category, $request);
    public function delete($category);
}
