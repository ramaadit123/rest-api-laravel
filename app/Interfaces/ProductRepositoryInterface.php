<?php

namespace App\Interfaces;

interface ProductRepositoryInterface
{
    public function getAllProducts($product);
    public function store($request);
    public function show($product);
    public function update($product, $request);
    public function delete($product);
}
