<?php

namespace App\Interfaces;

interface VariantRepositoryInterface
{
    public function getAllVariant($variant);
    public function store($product, $request);
    public function show($variant);
    public function update($variant, $request);
    public function delete($variant);
}
