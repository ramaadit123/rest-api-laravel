<?php

namespace App\Interfaces;

interface ImageRepositoryInterface
{
    public function getAllImage($image);
    public function store($product, $request);
    public function show($image);
    public function delete($image);
}
