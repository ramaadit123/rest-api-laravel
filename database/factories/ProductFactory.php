<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'category_id' => rand(1, 7),
            'user_id' => User::factory(),
            'name' => $name = $this->faker->sentence(),
            'slug' => str($name)->slug(),
            'description' => $this->faker->text(),
            'condition' => $this->faker->randomElement(['Baru', 'Bekas']),
            'price' => rand(50000, 1000000),
            'stock' => rand(10, 100),
            'thumbnail' => 'thumbnail',
        ];
    }
}
