<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Product;
use App\Models\Variant;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Product::factory(50)->create();
        User::factory(5)->hasProducts(15)->create();
        // Product::get()->each(function ($product) {
        //     $product->variants()->attach(
        //         Variant::get()->random(rand(1, 3))->pluck('id')
        //     );
        // });
    }
}
