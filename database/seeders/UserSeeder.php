<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect([
            [
                'name' => 'Rama Adhitya Setiadi',
                'email' => 'rama@gmail.com',
                'password' => bcrypt('password')
            ],
            [
                'name' => 'Rifa Putra Setiadi',
                'email' => 'rifa@gmail.com',
                'password' => bcrypt('password')
            ],
            [
                'name' => 'Bunga Agnia Setiadi',
                'email' => 'bunga@gmail.com',
                'password' => bcrypt('password')
            ],
        ])->each(fn ($q) => User::create($q));

        collect(['admin', 'seller'])->each(fn ($role) => Role::create(['name' => $role]));

        User::find(1)->roles()->attach([1]);
        User::find(2)->roles()->attach([2]);
    }
}
