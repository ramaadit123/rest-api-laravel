<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect([
            ['name' => $name = 'Baju Wanita', 'slug' => str($name)->slug()],
            ['name' => $name = 'Baju Pria', 'slug' => str($name)->slug()],
            ['name' => $name = 'Sepatu Wanita', 'slug' => str($name)->slug()],
            ['name' => $name = 'Sepatu Pria', 'slug' => str($name)->slug()],
            ['name' => $name = 'Tas Slempang Pria', 'slug' => str($name)->slug()],
            ['name' => $name = 'Tas Wanita', 'slug' => str($name)->slug()],
            ['name' => $name = 'Makanan', 'slug' => str($name)->slug()],
        ])->each(fn ($q) => Category::create($q));
    }
}
