<?php

namespace Database\Seeders;

use App\Models\Variant;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class VariantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect([
            [
                'product_id' => 1,
                'options' => 'Ukuran',
                'value' => 'M',
                'image' => 'image'
            ],
            [
                'product_id' => 1,
                'options' => 'Ukuran',
                'value' => 'M',
                'image' => 'image'
            ],
            [
                'product_id' => 1,
                'options' => 'Ukuran',
                'value' => 'M',
                'image' => 'image'
            ],
            [
                'product_id' => 1,
                'options' => 'Ukuran',
                'value' => 'M',
                'image' => 'image'
            ],
            [
                'product_id' => 1,
                'options' => 'Ukuran',
                'value' => 'M',
                'image' => 'image'
            ],
            [
                'product_id' => 2,
                'options' => 'Ukuran',
                'value' => 'M',
                'image' => 'image'
            ],
            [
                'product_id' => 2,
                'options' => 'Ukuran',
                'value' => 'M',
                'image' => 'image'
            ],
            [
                'product_id' => 2,
                'options' => 'Ukuran',
                'value' => 'M',
                'image' => 'image'
            ],
            [
                'product_id' => 2,
                'options' => 'Ukuran',
                'value' => 'M',
                'image' => 'image'
            ],
            [
                'product_id' => 2,
                'options' => 'Ukuran',
                'value' => 'M',
                'image' => 'image'
            ],
            [
                'product_id' => 3,
                'options' => 'Ukuran',
                'value' => 'M',
                'image' => 'image'
            ],
            [
                'product_id' => 3,
                'options' => 'Ukuran',
                'value' => 'M',
                'image' => 'image'
            ],
            [
                'product_id' => 3,
                'options' => 'Ukuran',
                'value' => 'M',
                'image' => 'image'
            ],
            [
                'product_id' => 3,
                'options' => 'Ukuran',
                'value' => 'M',
                'image' => 'image'
            ],
            [
                'product_id' => 3,
                'options' => 'Ukuran',
                'value' => 'M',
                'image' => 'image'
            ],
        ])->each(fn ($q) => Variant::create($q));
    }
}
